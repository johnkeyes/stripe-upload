# Error Uploading Files to Stripe from Django

Python Runtme: 3.6.2

1. Create virtualenv.
2. Install runtime requirements:  
  `pip install -r requirements.txt`
3. Run the dev server:  
  `STRIPE_API_KEY=sk_test_... python manage.py runserver 0.0.0.0:10001`
4. Open the form in a browser:  
  `http://localhost:10001/`
5. Select a file and press the Upload button.
6. Observe the UnicodeDecodeError
