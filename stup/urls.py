# -*- coding: utf-8 -*-  # noqa
import os
import stripe
from django.conf.urls import url
from django.http import HttpResponse
from django.template import loader
from django import forms


class FileForm(forms.Form):
    document = forms.FileField()


def upload_to_stripe(request):  # noqa

    if request.method == "POST":
        form = FileForm(files=request.FILES)
        if form.is_valid():
            stripe.api_key = os.getenv('STRIPE_API_KEY')
            document = form.cleaned_data['document']
            stripe.FileUpload.create(
                file=document,
                purpose="dispute_evidence",
            )

            return HttpResponse("OK")

        return HttpResponse("<for")
    else:
        # render the form
        form = FileForm()
        template = loader.get_template('file_upload.html')
        context = {
            'form': form
        }
        return HttpResponse(template.render(context, request))


urlpatterns = [
    url(r'^$', upload_to_stripe)
]
